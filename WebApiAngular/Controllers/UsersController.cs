﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using WebApiAngular.Models;

namespace WebApiAngular.Controllers
{
    public class UsersController : ApiController
    {
        public static Lazy<List<User>> Users = new Lazy<List<User>>();
        public static int PageLoadFlag = 1;
        public static int UId = 2;
        public static int loggedUID = -1;
        public UsersController()
        {
            if (PageLoadFlag == 1) {
                Users.Value.Add(new User { ID = 1, UID = "nmd", Pwd = "1234", FullName = "Nur Mohamamd", AboutU = "I'm Software Engineer", DOB = "12/12/1988" }); ;
                PageLoadFlag++;
            }
        }
        // GET api/users
        public HttpResponseMessage GetAllUsers()
        {
            if (loggedUID > 0) {//Get user info
                User user =  Users.Value.Find(p => p.ID == loggedUID);
                if (user != null) {                    
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "No data found.");
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized, "Access denied.");
        }
        
        // POST: api/Users
        public HttpResponseMessage AddUser(User user)
        {
            bool userExist = Users.Value.Find(p => p.UID == user.UID) != null;
            if (user != null && user.ID == 0 && !userExist) {//Add new user
                user.ID = UId;
                Users.Value.Add(user);
                UId++;
                HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.Created);
                return resp;
            } else if (user != null && user.ID == -1) {
                User u = Users.Value.Find(p => p.UID == user.UID);
                if (u == null) {
                    HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.NotFound);
                    return resp;
                }
                if (u.Pwd == user.Pwd) {//login
                    HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.OK);
                    loggedUID = u.ID;
                    return resp;
                } else {
                    HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.Unauthorized, "Access denied.");
                    return resp;
                }                
            } else {
                HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.InternalServerError, "No data to add.");
                return resp;
            }
        }
        // PUT: api/Users
        [HttpPut]
        public HttpResponseMessage UpdateUser(User user)
        {
            if (user != null && user.ID > 0) {//Update
                Users.Value.Find(p => p.ID == user.ID).FullName = user.FullName;
                HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.OK);
                return resp;
            } else if (user != null && user.ID == -1) {//Logout
                loggedUID = -1;
                HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.OK);
                return resp;
            } else {
                HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.InternalServerError, "No data to add.");
                return resp;
            }
        }

    }
}
