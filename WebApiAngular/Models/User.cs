﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngular.Models
{
    public class User
    {
        public String UID { get; set; }
        public String Pwd { get; set; }
        public int ID { get; set; }
        public String FullName { get; set; }
        public String DOB { get; set; }
        public String AboutU { get; set; }
    }
}