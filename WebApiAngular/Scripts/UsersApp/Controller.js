﻿app.controller('ApiController', function ($scope, APIService) {
           
    function getAllInfo() {
        var servCall = APIService.getUsers();
        servCall.then(function (success) {
            $scope.UserInfo = success;
            $scope.registerShow = false;
        }, function (error) {
            $scope.setError(error);
            console.log(error.statusText)
        });
    }
    $scope.registerUser = function (userid, userpwd, username, userdob, userabout) {
        var user = {
            ID: 0,
            DOB: userdob,
            UID: userid,
            PWD: userpwd,
            FullName: username,
            AboutU: userabout
        };
        var saveUser = APIService.saveUser(user);
        saveUser.then(function (success) {
            getAllInfo();
            $scope.statusModel = {
                messageType: "success",
                code: success.status,
                message: success.statusText,
                messageFor: "register"
            };
            
        }, function (error) {
            error.data = "register";
            $scope.setError(error);
            console.log(error.statusText)
        })
    };

    $scope.makeEditable = function (obj) {
        obj.target.setAttribute("contenteditable", true);
        obj.target.focus();
    };
    $scope.updUser = function (user, eve) {
        console.log($scope.FullName);
        user.FullName = eve.currentTarget.innerText;
        var upd = APIService.updateUser(user);
        upd.then(function (success) {
            getAllInfo();
        }, function (error) {
            $scope.setError(error);
            console.log(error.statusText)
        })
    };
    
    $scope.loginUser = function (uid, pwd) {        
        var user = {
            UID: uid,
            PWD: pwd,
            ID: -1
        };
        var loginU = APIService.saveUser(user);
        loginU.then(function (success) {
            $scope.uid = "";
            $scope.upwd = "";
            $scope.loginSuccess = true;
            $scope.loggedID = user.UID;
            $scope.registerShow = false;
            $scope.statusModel = {
                messageType: "success",
                code: success.status,
                message: success.statusText,
                messageFor: "login"
            };
            getAllInfo();
            
        }, function (error) {
            error.data = "login";
            $scope.setError(error);
            console.log(error.statusText)
        })
    }
    $scope.logOut = function () {
        $scope.loginSuccess = false;
        $scope.UserInfo.ID = -1;
        var user = $scope.UserInfo;
        var upd = APIService.updateUser(user);
        upd.then(function (success) {
            getAllInfo();
        }, function (error) {
            $scope.setError(error);
            console.log(error.statusText)
        })
    }
    $scope.registerU = function () {
        $scope.registerShow = true;
    }
    $scope.setError = function (error) {
        $scope.statusModel = {
            messageType: "error",
            code: error.status,
            message: error.statusText,
            messageFor: error.data
        };
        
    }

});