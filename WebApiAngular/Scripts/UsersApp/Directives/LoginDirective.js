﻿
angular.module('APIModule').directive('printLogin', function () {
    return {
        restrict: 'E',
        scope: {
            statusModel: '=',
            loginClick: '&'
        },
        templateUrl: 'Scripts/UsersApp/Templates/LoginTemplate.html',

        link: function (scope, el, attr) {
            scope.loginUser = function (uid, pwd) {
                scope.loginClick({ userID: uid, loginPwd: pwd });
                scope.uid = "";
                scope.upwd = "";
            }
        }
    }
});
