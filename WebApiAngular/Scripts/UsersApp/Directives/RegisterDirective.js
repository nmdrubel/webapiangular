﻿
angular.module('APIModule').directive('printRegister', function () {
    return {
        restrict: 'E',
        scope: {
            statusModel: '=',
            registerClick: '&'
        },
        templateUrl: 'Scripts/UsersApp/Templates/RegisterTemplate.html',

        link: function (scope, el, attr) {
            scope.onClickRegisterUser = function (uid, upwd, uname, udob, uabout) {
                scope.registerClick({ userID: uid, loginPwd: upwd, userName: uname, userDOB: udob, userAbout: uabout }); 
                scope.userid = "";
                scope.userpwd = "";
                scope.username = "";
                scope.userdob = "";
                scope.userabout = "";
            }
        }
    }
});
