﻿app.service("APIService", function ($http) {
    this.getUsers = function () {
        var url = 'api/Users';
        return $http.get(url).then(function (response) {
            return response.data;
        });
    }
    this.saveUser = function (user) {
        return $http({
            method: 'post',
            data: user,
            url: 'api/Users'
        });
    }
    this.updateUser = function (user) {
        return $http({
            method: 'put',
            data: user,
            url: 'api/Users'
        });
    }
    this.deleteUser = function (uID) {
        var url = 'api/Users/' + uID;
        return $http({
            method: 'delete',
            data: uID,
            url: url
        });
    }
    
});